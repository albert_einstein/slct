
proto:
	protoc --go_out=plugins=grpc:. *.proto

build:
	CGO_CFLAGS="-I/home/artem/my/slct/rocksdb/include" CGO_LDFLAGS="-L/home/artem/my/slct/rocksdb -lrocksdb -lstdc++ -lm -lz -lbz2 -lsnappy -llz4 -lzstd" go build -tags netgo -ldflags "-w -extldflags \"-static\"" ./cmd/.

run:
	CGO_CFLAGS="-I/home/artem/my/slct/rocksdb/include" CGO_LDFLAGS="-L/home/artem/my/slct/rocksdb -lrocksdb -lstdc++ -lm -lz -lbz2 -lsnappy -llz4 -lzstd" go run ./cmd/.