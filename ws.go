package slct

import (
	"context"
	"fmt"
	"io"

	grpc "google.golang.org/grpc"
)

func ManageDefs(ctx context.Context, conn *grpc.ClientConn, group string, defs Definitions) error {
	client := NewSlctClient(conn)
	stream, err := client.ListenUpdates(context.Background(), &ListenRequest{Group: group})
	if err != nil {
		return fmt.Errorf("listen updates: %v", err)
	}
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return fmt.Errorf("eof")
		}
		if err != nil {
			return fmt.Errorf("recv: %v", err)
		}
		// d, _ := json.MarshalIndent(req, "", " ")
		// log.Print("before", string(d))
		go func(req *UnblockedState) {
			req.State.RemoveSelect(req.Select.Id)
			err := req.State.Exec(defs, req.Select)
			if err != nil {
				panic(err)
			}
			// d, _ := json.MarshalIndent(req.State, "", " ")
			// log.Print("after", string(d))
			_, err = client.UpdateState(context.Background(), &UpdateRequest{ClientId: req.ClientId, State: req.State})
			if err != nil {
				panic(err)
			}
		}(req)
	}
}
