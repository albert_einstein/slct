module gitlab.com/albert_einstein/slct

go 1.13

require (
	github.com/danilopolani/gocialite v1.0.2 // indirect
	github.com/dgraph-io/badger v1.6.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/facebookgo/ensure v0.0.0-20160127193407-b4ab57deab51 // indirect
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/facebookgo/subset v0.0.0-20150612182917-8dac2c3c4870 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.3.2
	github.com/google/btree v1.0.0
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/websocket v1.4.1
	github.com/lib/pq v1.3.0
	github.com/lni/dragonboat-example/v3 v3.0.0-20191025144745-471f9a8fc347
	github.com/lni/dragonboat/v3 v3.1.4
	github.com/logpacker/PayPal-Go-SDK v2.0.5+incompatible // indirect
	github.com/lunixbochs/struc v0.0.0-20190916212049-a5c72983bc42
	github.com/mailru/easyjson v0.7.1
	github.com/olivere/elastic/v7 v7.0.10 // indirect
	github.com/plutov/paypal v2.0.5+incompatible // indirect
	github.com/sideshow/apns2 v0.20.0 // indirect
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/tecbot/gorocksdb v0.0.0-20191217155057-f0fad39f321c
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
	google.golang.org/appengine v1.4.0
	google.golang.org/grpc v1.27.1
	gopkg.in/oleiade/reflections.v1 v1.0.0 // indirect
)
