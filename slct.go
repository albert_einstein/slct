package slct

import (
	"bytes"
	"encoding/binary"

	proto "github.com/golang/protobuf/proto"
)

type SelReq struct {
	State  State
	Select Select
}

func (s Select) UKey() []byte {
	b := make([]byte, 8, 9+len(s.Group))
	binary.BigEndian.PutUint64(b, uint64(s.Seq))
	return bytes.Join([][]byte{[]byte(s.Group), b}, []byte{0})
}

func (s Select) BKey() []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(s.Seq))
	return b
}

func (s *State) Data() []byte {
	data, err := proto.Marshal(s)
	if err != nil {
		panic(err)
	}
	return data
}

func (s *Select) Data() []byte {
	data, err := proto.Marshal(s)
	if err != nil {
		panic(err)
	}
	return data
}

func (c *Channel) Data() []byte {
	data, err := proto.Marshal(c)
	if err != nil {
		panic(err)
	}
	return data
}

func (s *State) HasSelect(id string) bool {
	for _, sel := range s.Selects {
		if sel.Id == id {
			return true
		}
	}
	return false
}
func (s *State) SetSelect(new *Select) (created bool) {
	for i, sel := range s.Selects {
		if sel.Id == new.Id {
			s.Selects[i] = new
			return false
		}
	}
	s.Selects = append(s.Selects, new)
	return true
}

func (s *State) NewSelects(old *State, uSel *Select) []*Select {
	if old == nil {
		return s.Selects
	}
	var ss []*Select
	for _, sel := range s.Selects {
		if uSel != nil && sel.Id == uSel.Id {
			ss = append(ss, sel)
		} else if !old.HasSelect(sel.Id) {
			ss = append(ss, sel)
		}
	}
	return ss
}

func (s *State) RemoveSelect(id string) {
	for i, sel := range s.Selects {
		if sel.Id == id {
			s.Selects = append(s.Selects[:i], s.Selects[i+1:]...)
			return
		}
	}
}
