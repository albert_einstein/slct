### Don't communicate by sharing memory. Share memory by communicating (c)

This service implements Go's CSP persistent runtime and provides API for **select** statement with linearizability guarantees. This makes communication between services atomic, reliable and asynchronous.


