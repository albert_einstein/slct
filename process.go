package slct

import (
	"encoding/json"
	"fmt"
	"time"
)

// Process is a syntax sugar API for managing selects.
// All what it's doing - is creating []Selects that will be returned from state
type Process struct {
	resumed *Select
	state   *State
}

type Definition interface {
	Resume(d []byte, p *Process) ([]byte, error)
}

// Map of definitions that will route *Resume* state
// to call a definition with the same Name
type Definitions map[string]Definition

// This updates the state using Definition and select that was unblocked
func (s *State) Exec(defs Definitions, sel *Select) (err error) {
	def, ok := defs[s.Name]
	if !ok {
		return fmt.Errorf("definition not found for %v", s.Name)
	}
	newState, err := def.Resume(s.State, &Process{
		resumed: sel,
		state:   s,
	})
	if err != nil {
		return err
	}
	s.State = newState
	return nil
}

// Name is a process name (not ID).
// This should be used to uniquely identify definition
func (p *Process) Name() string {
	return p.state.Name
}

// Goroutine returns a goroutine that was unblocked and should be resumed.
func (p *Process) Goroutine() string {
	return p.resumed.Id
}

// Retuns the FSM status of unblocked select and specifies where you should
// resume your goroutine
func (p *Process) Status() string {
	return p.resumed.ToStatus
}

func (p *Process) GetResult(dst ...interface{}) bool {
	if p.resumed.Status == Select_Closed {
		return false
	}
	if p.resumed.Status != Select_OK {
		panic("non OK status in RecvResult")
	}

	var temp []json.RawMessage
	err := json.Unmarshal(p.resumed.RecvData, &temp)
	if err != nil {
		panic(err)
	}
	if len(temp) != len(dst) {
		panic(fmt.Sprintf("param count mismatch: %v %v", len(temp), len(dst)))
	}
	for i := range temp {
		err := json.Unmarshal(temp[i], dst[i])
		if err != nil {
			panic(err)
		}
	}
	return true
}

func (p *Process) lastSel() *Select {
	for i, sel := range p.state.Selects {
		if sel.Id == p.resumed.Id {
			return p.state.Selects[i]
		}
	}
	sel := &Select{
		Status: Select_Waiting,
		State:  p.state.Id,
		Group:  p.resumed.Group,
		Id:     p.resumed.Id,
	}
	p.state.SetSelect(sel)
	return sel
}

func (p *Process) To(name string) *Process {
	ls := p.lastSel()
	ls.Cases[len(ls.Cases)-1].ToStatus = name
	return p
}

func (p *Process) Go(id string) *Process {
	s := Select{
		Id:     id,
		Status: Select_OK,
		Cases: []*Case{
			{
				Op: Case_Default,
			},
		},
	}
	created := p.state.SetSelect(&s)
	if !created {
		panic("starting goroutine twice")
	}
	return p
}

func (p *Process) Recv(channel string) *Process {
	p.checkNoDefault()
	ls := p.lastSel()
	ls.Cases = append(ls.Cases, &Case{
		Chan: channel,
		Op:   Case_Recv,
	})
	return p
}

func (p *Process) RecvL(channel string) *Process {
	return p.Recv(fmt.Sprintf("local_state_%v_chan_%v", p.state.Id, channel))
}

func (p *Process) Send(channel string, data ...interface{}) *Process {
	p.checkNoDefault()
	d, err := json.Marshal(data)
	if err != nil {
		panic(err)
	}
	ls := p.lastSel()
	ls.Cases = append(ls.Cases, &Case{
		Chan: channel,
		Op:   Case_Send,
		Data: d,
	})
	return p
}

func (p *Process) SendL(channel string, data ...interface{}) *Process {
	return p.Send(fmt.Sprintf("local_state_%v_chan_%v", p.state.Id, channel))
}

func (p *Process) checkNoDefault() {
	for _, c := range p.lastSel().Cases {
		if c.Op == Case_Default {
			panic("default case should always be the last one")
		}
	}
}

func (p *Process) Default() *Process {
	p.checkNoDefault()
	ls := p.lastSel()
	ls.Cases = append(ls.Cases, &Case{
		Op: Case_Default,
	})
	return p
}

func (p *Process) After(after time.Duration) *Process {
	return p.At(time.Now().Add(after))
}

func (p *Process) At(at time.Time) *Process {
	p.checkNoDefault()
	ls := p.lastSel()
	ls.Cases = append(ls.Cases, &Case{
		Op:   Case_Send,
		Time: uint64(at.Unix()),
	})
	return p
}
