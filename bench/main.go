package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"sync"
	"time"

	"gitlab.com/albert_einstein/slct"
	"google.golang.org/grpc"
)

const G_Main = "main"
const G_Other = "other"

const Main_Start = "start"
const Main_Waiting = "waiting"
const Main_Done = "done"

type CounterDefinition struct{}

func (cd CounterDefinition) Resume(state []byte, p *slct.Process) ([]byte, error) {
	c := CounterProcess{}
	err := json.Unmarshal(state, &c)
	if err != nil {
		return nil, err
	}
	switch p.Goroutine() {
	case G_Main:
		switch p.Status() {
		case Main_Start:
			err = c.Main_Start(p)
		case Main_Waiting:
			err = c.Main_Waiting(p)
		case Main_Done:
			err = c.Main_Done(p)
		default:
			err = fmt.Errorf("status '%v' not found", p.Status())
		}
	default:
		err = fmt.Errorf("goroutine '%v' not found", p.Goroutine())
	}
	if err != nil {
		return nil, err
	}
	return json.Marshal(c)
}

var defs = slct.Definitions{
	"counter": &CounterDefinition{},
}

type CounterProcess struct {
	Counter int64
}

func (c *CounterProcess) Main_Start(p *slct.Process) error {
	c.Counter = 0
	mu.Lock()
	output++
	mu.Unlock()
	p.Default().To(Main_Waiting)
	return nil
}

func (c *CounterProcess) Main_Waiting(p *slct.Process) error {
	c.Counter++
	mu.Lock()
	output++
	mu.Unlock()
	if c.Counter >= 2 {
		p.Default().To(Main_Done)
	} else {
		p.Default().To(Main_Waiting)
	}
	return nil
}

func (c *CounterProcess) Main_Done(p *slct.Process) error {
	mu.Lock()
	output++
	mu.Unlock()
	return nil
}

var mu sync.Mutex
var input = int64(0)
var lat = int64(0)
var output = int64(0)
var start = time.Now()

var client slct.SlctClient

func main() {
	conn, err := grpc.Dial(":9090", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	client = slct.NewSlctClient(conn)
	rand.Seed(time.Now().Unix())
	var wg sync.WaitGroup

	for i := 0; i < 150; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			bbb("1")
		}(i)
	}

	for i := 0; i < 50; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			err := process("1")
			if err != nil {
				log.Fatal(err)
			}
		}(i)
	}
	wg.Wait()
}

func bbb(g string) {
	for {
		stateID := strconv.Itoa(rand.Intn(1000000000)) + strconv.Itoa(int(time.Now().UnixNano()))
		// sendc := strconv.Itoa(rand.Intn(10))
		// recvc := strconv.Itoa(rand.Intn(10) + 11)
		// if rand.Int63()%2 == 0 {
		// 	recvc = strconv.Itoa(rand.Intn(10))
		// 	sendc = strconv.Itoa(rand.Intn(10) + 11)
		// }

		f := time.Now()
		_, err := client.NewState(context.Background(), &slct.State{
			Id:    stateID,
			Name:  "counter",
			State: []byte("{}"),
			Selects: []*slct.Select{
				&slct.Select{
					Status: slct.Select_Waiting,
					Group:  g,
					State:  stateID,
					Id:     G_Main,
					Cases: []*slct.Case{
						{
							Op:       slct.Case_Default,
							ToStatus: Main_Start,
						},
					},
				},
			},
		})
		if err != nil {
			panic(err)
		}

		mu.Lock()
		input++
		lat += time.Since(f).Milliseconds()
		if time.Since(start) > time.Second*1 {
			log.Printf("in: %v out: %v req/s lat: %vms input:%v output:%v ", float64(input)/time.Since(start).Seconds(), float64(output)/time.Since(start).Seconds(), lat/input, input, output)
			input = 0
			output = 0
			lat = 0
			start = time.Now()
		}
		mu.Unlock()
	}
}

func process(group string) error {
	conn, err := grpc.Dial(":9090", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	return slct.ManageDefs(context.Background(), conn, "1", defs)
}
