package main

// log.Print("STARTED")
// 	start := time.Now()
// 	var total int64 = 0
// 	go func() {
// 		t := time.NewTicker(time.Second * 5)
// 		for range t.C {
// 			tt := atomic.LoadInt64(&total)
// 			atomic.AddInt64(&total, -tt)
// 			log.Println(r.seq, float64(tt)/time.Since(start).Seconds(), "req/sec", "selects:", len(r.Selects), "pending", len(r.states), "total", tt)
// 			start = time.Now()
// 		}
// 	}()

// 	rand.Seed(time.Now().Unix())
// 	var wg sync.WaitGroup
// 	for threads := 0; threads < 1000; threads++ {
// 		wg.Add(1)
// 		go func() {
// 			defer wg.Done()
// 			for j := 0; j < 50000; j++ {
// 				stateID := strconv.Itoa(rand.Intn(1000000000)) + strconv.Itoa(int(time.Now().UnixNano()))
// 				sendc := strconv.Itoa(rand.Intn(10))
// 				recvc := strconv.Itoa(rand.Intn(10) + 11)
// 				if rand.Int63()%2 == 0 {
// 					recvc = strconv.Itoa(rand.Intn(10))
// 					sendc = strconv.Itoa(rand.Intn(10) + 11)
// 				}

// 				sss := time.Now()
// 				err := r.NewState(&State{
// 					Id: stateID,
// 					Selects: []*Select{
// 						&Select{
// 							Group: "g",
// 							State: stateID,
// 							Id:    stateID + "1",
// 							Cases: []*Case{
// 								{
// 									Op:   Case_Send,
// 									Chan: sendc,
// 								},
// 								{
// 									Op:   Case_Recv,
// 									Chan: recvc,
// 								},
// 							},
// 						},
// 					},
// 				})
// 				if atomic.AddInt64(&total, 1)%100000 == 0 {
// 					log.Print("Latency:", time.Since(sss).Milliseconds())
// 				}
// 				if err != nil {
// 					log.Printf("select: %v", err)
// 				}
// 			}
// 		}()
// 	}
// 	wg.Wait()
// 	log.Printf("FINISHED %d ms", time.Since(start).Milliseconds())
// 	cancel()
// 	swg.Wait()
