package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/websocket"
	"github.com/tecbot/gorocksdb"
	"gitlab.com/albert_einstein/slct"
)

type Proc struct {
	cancel   func()
	handlers map[string]StateHandler
}

type Server struct {
	r       *SelectRuntime
	mu      sync.Mutex
	procs   map[string]*Proc
	clients map[string]*Client
}

type Client struct {
	mu      sync.Mutex
	results map[string]chan *slct.State // TODO: cleanup
}

func (srv *Server) HTTPNewState(w http.ResponseWriter, r *http.Request) {
	var s slct.State
	err := json.NewDecoder(r.Body).Decode(&s)
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintf(w, "json: %v", err)
		return
	}
	err = srv.r.NewState(&s)
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintf(w, "new state: %v", err)
		return
	}
}

func (srv *Server) HTTPUpdateState(w http.ResponseWriter, r *http.Request) {
	var req slct.UpdateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintf(w, "json: %v", err)
		return
	}
	_, err = srv.UpdateState(r.Context(), &req)
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintf(w, "update state: %v", err)
		return
	}
}

var upgrader = websocket.Upgrader{}

func (srv *Server) WSListenUpdates(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintf(w, "upgrade: %v", err)
		return
	}
	err = srv.listenUpdates(r.Context(),
		&slct.ListenRequest{Group: r.URL.Query().Get("group")},
		func(s *slct.UnblockedState) error {
			return c.WriteJSON(s)
		})
	if err != nil {
		w.WriteHeader(400)
		fmt.Fprintf(w, "listen updates: %v", err)
		return
	}

}

func (srv *Server) NewState(ctx context.Context, s *slct.State) (*slct.Empty, error) {
	return &slct.Empty{}, srv.r.NewState(s)
}

func (srv *Server) UpdateState(ctx context.Context, req *slct.UpdateRequest) (*slct.Empty, error) {
	srv.mu.Lock()
	defer srv.mu.Unlock()
	c, ok := srv.clients[req.ClientId]
	if !ok {
		return &slct.Empty{}, fmt.Errorf("can't find client with id: %v", req.ClientId)
	}
	c.mu.Lock()
	defer c.mu.Unlock()
	cb, ok := c.results[req.State.Id]
	if !ok {
		return &slct.Empty{}, fmt.Errorf("can't find state with id: %v", req.State.Id)
	}
	select {
	case cb <- req.State:
		delete(c.results, req.State.Id)
		return &slct.Empty{}, nil
	case <-time.After(time.Second * 5): // external server go and processed request, but we failed to wait for callback
		return &slct.Empty{}, fmt.Errorf("callback closed")
	}
}

func (srv *Server) ListenUpdates(r *slct.ListenRequest, stream slct.Slct_ListenUpdatesServer) error {
	return srv.listenUpdates(stream.Context(), r, stream.Send)
}

func (srv *Server) listenUpdates(ctx context.Context, r *slct.ListenRequest, send func(*slct.UnblockedState) error) error {
	id := fmt.Sprint(rand.Int63())
	c := &Client{
		results: map[string]chan *slct.State{},
	}
	srv.mu.Lock()
	srv.clients[id] = c
	srv.mu.Unlock()

	handler := func(req slct.SelReq) (slct.State, error) {
		cb := make(chan *slct.State)

		c.mu.Lock()
		c.results[req.State.Id] = cb
		c.mu.Unlock()

		err := send(&slct.UnblockedState{ClientId: id, State: &req.State, Select: &req.Select})
		if err != nil {
			c.mu.Lock()
			delete(c.results, req.State.Id)
			c.mu.Unlock()
			return slct.State{}, err
		}
		select {
		case s := <-cb:
			return *s, nil
		case <-ctx.Done():
			c.mu.Lock()
			delete(c.results, req.State.Id)
			c.mu.Unlock()
			return slct.State{}, fmt.Errorf("ws closed")
		}
	}

	srv.mu.Lock()
	g, ok := srv.procs[r.Group]
	if !ok {
		cctx, ccancel := context.WithCancel(context.Background())
		srv.procs[r.Group] = &Proc{
			cancel: ccancel,
			handlers: map[string]StateHandler{
				id: handler,
			},
		}
		g = srv.procs[r.Group]
		go srv.r.processUpdates(cctx, r.Group, 5000, func(req slct.SelReq) (slct.State, error) {
			srv.mu.Lock()
			g, ok := srv.procs[r.Group]
			if !ok {
				srv.mu.Unlock()
				return slct.State{}, fmt.Errorf("no registered group")
			}
			if len(g.handlers) == 0 {
				srv.mu.Unlock()
				return slct.State{}, fmt.Errorf("no handlers")
			}
			for _, v := range g.handlers { // send to random handler (for now)
				srv.mu.Unlock()
				return v(req)
			}
			srv.mu.Unlock()
			return slct.State{}, fmt.Errorf("no available handlers")
		})
	} else {
		_, ok = g.handlers[id]
		if ok {
			srv.mu.Unlock()
			return fmt.Errorf("already has listener with id %v", id)
		}
		g.handlers[id] = handler
	}
	srv.mu.Unlock()

	<-ctx.Done()
	srv.mu.Lock()
	delete(srv.clients, id)
	delete(g.handlers, id)
	if len(g.handlers) == 0 {
		g.cancel()
		delete(srv.procs, r.Group)
	}
	srv.mu.Unlock()
	return nil
}

func (p *streamer) processSelect(sel slct.Select) bool {
	r := p.runtime
	r.lockState(sel.State)
	old := r.mustGetState(sel.State)
	s, err := p.handler(slct.SelReq{State: old, Select: sel}) // TODO: validate response (versions)
	if err != nil {
		r.unlockState(sel.State)
		log.Print("state %v: %v", sel.State, err)
		time.Sleep(time.Second * 5)
		return true
	}

	s.Version++
	toCreate := s.NewSelects(&old, &sel)
	r.batchMu.Lock()
	r.ss = append(r.ss, &s)
	r.dd = append(r.dd, &sel)
	for _, sel := range toCreate {
		r.cc = append(r.cc, sel)
	}
	cb := r.curChan
	r.batchMu.Unlock()
	<-cb
	r.unlockState(sel.State)
	return true

}

func (p *streamer) processState(id string) {
	for {
		p.mu.Lock()
		ss := p.stateQueues[id]
		if len(ss) == 0 {
			panic("race condition in workers")
		}
		p.mu.Unlock()

		for !p.processSelect(ss[0]) {
		}
		p.mu.Lock()
		ss = p.stateQueues[id][1:]
		p.stateQueues[id] = ss
		if len(ss) == 0 {
			delete(p.stateQueues, id)
			p.mu.Unlock()
			return
		}
		p.mu.Unlock()
	}
}

func (r *SelectRuntime) processUpdates(ctx context.Context, group string, pool int, h StateHandler) {
	log.Print("START")
	defer log.Print("FINISH")
	opts := gorocksdb.NewDefaultReadOptions()
	opts.SetFillCache(false)
	prefix := append([]byte(group), byte(0))
	lastKey := append([]byte(group), byte(0))
	p := &streamer{
		handler:     h,
		runtime:     r,
		stateQueues: map[string][]slct.Select{},
	}
	var wg sync.WaitGroup
	for {
		it := p.runtime.db.NewIteratorCF(p.runtime.ro, p.runtime.cfhUnblockedSelects)
		for it.Seek(lastKey); it.ValidForPrefix(prefix); it.Next() {
			select {
			case <-ctx.Done():
				wg.Wait() // wait for all processors to finish
				return
			default:
			}
			if bytes.Equal(it.Key().Data(), lastKey) {
				it.Key().Free()
				it.Value().Free()
				continue
			}
			var sel slct.Select
			err := proto.Unmarshal(it.Value().Data(), &sel)
			if err != nil {
				panic(err)
			}
			for {
				p.mu.Lock()
				if len(p.stateQueues) > pool { // worker pool limit reached
					p.mu.Unlock()
					select {
					case <-ctx.Done():
						wg.Wait() // wait for all processors to finish
						return
					default:
					}
					time.Sleep(time.Millisecond * 100)
					continue
				}
				ss := p.stateQueues[sel.State]
				p.stateQueues[sel.State] = append(ss, sel) // add tasks to state queue
				p.mu.Unlock()
				if len(ss) == 0 {
					wg.Add(1)
					go func(sel slct.Select) {
						defer wg.Done()
						p.processState(sel.State) // create new worker for processing this state
					}(sel)
				}
				break
			}

			if len(lastKey) != it.Key().Size() {
				lastKey = make([]byte, it.Key().Size())
			}
			copy(lastKey, it.Key().Data()) // we should copy data, as we will free this later
			it.Key().Free()
			it.Value().Free()
		}
		it.Close()

		time.Sleep(time.Millisecond * 100) // TODO: dynamic wait time?, i.e. based on number of searches with no success.
	}
}
