package main

import (
	"context"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gorilla/mux"
	"gitlab.com/albert_einstein/slct"
	"google.golang.org/grpc"
)

func main() {
	log.Print("starting server")
	r, err := NewSelectRuntime("./db/7/")
	if err != nil {
		log.Fatal(err)
	}

	s := &Server{
		r:       r,
		procs:   map[string]*Proc{},
		clients: map[string]*Client{},
	}

	go func() {
		lis, err := net.Listen("tcp", ":9090")
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}
		server := grpc.NewServer()
		slct.RegisterSlctServer(server, s)
		err = server.Serve(lis)
		if err != nil {
			log.Fatal(err)
		}
	}()

	go func() {
		r := mux.NewRouter()
		//r.HandleFunc("/state", s.HTTPGetState).Methods("GET")
		r.HandleFunc("/state", s.HTTPNewState).Methods("POST")
		r.HandleFunc("/state", s.HTTPUpdateState).Methods("PUT")
		r.HandleFunc("/ws", s.WSListenUpdates).Methods("GET")
		srv := http.Server{
			Addr: ":8080",
		}
		err := srv.ListenAndServe()
		if err != nil {
			log.Fatal(err)
		}
	}()

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT)
		<-sigs
		cancel()
	}()

	err = r.Run(ctx)
	if err != nil {
		log.Fatal(err)
	}
	log.Print("server shut down")
}
