package main

import (
	"context"
	"encoding/binary"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/google/btree"
	"github.com/tecbot/gorocksdb"
	"gitlab.com/albert_einstein/slct"
)

type SelectRuntime struct {
	// rocksdb stuff
	db                  *gorocksdb.DB
	wo                  *gorocksdb.WriteOptions
	ro                  *gorocksdb.ReadOptions
	cfhDefault          *gorocksdb.ColumnFamilyHandle
	cfhChannels         *gorocksdb.ColumnFamilyHandle
	cfhBlockedSelects   *gorocksdb.ColumnFamilyHandle
	cfhUnblockedSelects *gorocksdb.ColumnFamilyHandle
	cfhStates           *gorocksdb.ColumnFamilyHandle

	stateMu sync.Mutex
	states  map[string]struct{} // state locks

	batchMu sync.Mutex
	curChan chan struct{}  // callbacks for this batch
	cc      []*slct.Select // selects for this batch
	dd      []*slct.Select // unprocessed selects to delete
	ss      []*slct.State  // states for this batch

	seq     uint64     // last seq
	batches chan batch // used to process next selects, while current ones are flushed to db

	// in-memory lookup for frequently accessed data
	Selects   map[sid]*slct.Select // blocked selects
	Chans     map[string]channel   // channels index, recv and send
	TimeChans *btree.BTree         // index for time selects
	wg        sync.WaitGroup
}

type StateHandler func(r slct.SelReq) (slct.State, error)

// streamer reads unblocked selects in sequential order and
// executes them in a worker pool
// we have to make sure that selects for the same state are always
// processed in sequential order and are not processed simultaneously
type streamer struct {
	mu          sync.Mutex
	stateQueues map[string][]slct.Select
	handler     StateHandler
	runtime     *SelectRuntime
}

type sid struct {
	Select string
	State  string
}

// Creates(or restores) runtime stored at 'dbpath'
func NewSelectRuntime(dbpath string) (*SelectRuntime, error) {
	dbOpts := gorocksdb.NewDefaultOptions()
	dbOpts.IncreaseParallelism(2)
	dbOpts.SetCreateIfMissing(true)
	dbOpts.SetCreateIfMissingColumnFamilies(true)
	dbOpts.SetCompression(gorocksdb.LZ4Compression)
	dbOpts.SetWALRecoveryMode(gorocksdb.PointInTimeRecovery)

	chanOpts := gorocksdb.NewDefaultOptions()
	chanOpts.OptimizeForPointLookup(500)

	defOpts := gorocksdb.NewDefaultOptions()
	defOpts.SetCompression(gorocksdb.LZ4Compression)

	bselOpts := gorocksdb.NewDefaultOptions()
	bselOpts.SetCompression(gorocksdb.LZ4Compression)

	uselOpts := gorocksdb.NewDefaultOptions()
	uselOpts.SetCompression(gorocksdb.LZ4Compression)

	stateOpts := gorocksdb.NewDefaultOptions()
	stateOpts.SetCompression(gorocksdb.LZ4Compression)
	stateOpts.OptimizeForPointLookup(500)

	db, cfh, err := gorocksdb.OpenDbColumnFamilies(dbOpts, dbpath, []string{
		"default",
		"channels",
		"blocked_selects",
		"unblocked_selects",
		"state_index"}, []*gorocksdb.Options{defOpts, chanOpts, bselOpts, uselOpts, stateOpts})
	if err != nil {
		return nil, err
	}

	if len(cfh) != 5 {
		return nil, fmt.Errorf("wrong number of chfs")
	}
	r := &SelectRuntime{
		db:                  db,
		ro:                  gorocksdb.NewDefaultReadOptions(),
		wo:                  gorocksdb.NewDefaultWriteOptions(),
		cfhDefault:          cfh[0],
		cfhChannels:         cfh[1],
		cfhBlockedSelects:   cfh[2],
		cfhUnblockedSelects: cfh[3],
		cfhStates:           cfh[4],
		Selects:             map[sid]*slct.Select{},
		Chans:               map[string]channel{},
		states:              map[string]struct{}{},
		batches:             make(chan batch, 1),
		curChan:             make(chan struct{}),
		TimeChans:           btree.New(32),
	}
	r.wo.SetSync(true)

	// load channels
	ro := gorocksdb.NewDefaultReadOptions()
	ro.SetFillCache(false)
	it := r.db.NewIteratorCF(r.ro, r.cfhChannels)
	for it.SeekToFirst(); it.Valid(); it.Next() {
		var c slct.Channel
		err := proto.Unmarshal(it.Value().Data(), &c)
		if err != nil {
			return nil, err
		}
		r.Chans[c.Id] = channel{
			Channel: c,
		}
		it.Key().Free()
		it.Value().Free()
	}
	it.Close()

	// load selects
	it = r.db.NewIteratorCF(r.ro, r.cfhBlockedSelects)
	for it.SeekToFirst(); it.Valid(); it.Next() {
		var s slct.Select
		err := proto.Unmarshal(it.Value().Data(), &s)
		if err != nil {
			return nil, err
		}
		r.addSelect(&s)
		it.Key().Free()
		it.Value().Free()
	}
	it.Close()

	// load seq
	bseq, err := r.db.Get(r.ro, []byte("seq"))
	if err != nil {
		return nil, err
	}
	if bseq.Size() > 0 {
		r.seq = binary.BigEndian.Uint64(bseq.Data())
	}
	log.Printf("loaded. blocked:%v channels:%v seq:%v", len(r.Selects), len(r.Chans), r.seq)
	return r, nil
}

func (r *SelectRuntime) Run(ctx context.Context) error {
	go func() { // cut batches, process them and schedule for flushing to disk
		total := 0
		start := time.Now()
		for {
			select {
			case <-ctx.Done():
				log.Print("shutting down")
				close(r.batches)
				r.wg.Wait()
				return

			default:
				r.batchMu.Lock()
				states := r.ss
				selects := r.cc
				processed := r.dd
				r.ss = nil
				r.cc = nil
				r.dd = nil
				cc := r.curChan
				r.curChan = make(chan struct{})
				r.batchMu.Unlock()

				wb := gorocksdb.NewWriteBatch()
				for _, s := range states {
					total++

					wb.PutCF(r.cfhStates, []byte(s.Id), s.Data())
					wb.PutCF(r.cfhStates, []byte(s.Id+"_"+fmt.Sprint(s.Version)), s.Data())
				}
				if time.Since(start) > time.Second*5 {
					log.Printf("%v req/s of %v processed", float64(total)/time.Since(start).Seconds(), total)
					total = 0
					start = time.Now()
				}
				for _, sel := range selects {
					r.handleSelect(wb, sel)
				}
				r.handleTick(wb)
				for _, sel := range processed {
					wb.DeleteCF(r.cfhUnblockedSelects, sel.UKey())
				}
				if wb.Count() == 0 {
					wb.Destroy()
					time.Sleep(time.Millisecond * 1)
					continue
				}

				wb.Put([]byte("seq"), buint64(r.seq))
				r.wg.Add(1)
				r.batches <- batch{
					wb: wb,
					cc: cc,
				}
			}
		}
	}()

	for b := range r.batches { // flush batches to disk
		err := r.db.Write(r.wo, b.wb)
		if err != nil {
			panic(fmt.Errorf("db write: %v", err))
		}
		b.wb.Destroy()
		close(b.cc)
		r.wg.Done()
	}
	r.cfhDefault.Destroy()
	r.cfhChannels.Destroy()
	r.cfhBlockedSelects.Destroy()
	r.cfhUnblockedSelects.Destroy()
	r.cfhStates.Destroy()
	r.db.Close()
	return nil
}

// NewState creates or updates state in Runtime.
// For updates - you should always use Stream API and listen for unblocked selects
// Updates for this API is for fixing issues only.
// It's not possible to remove selects made by states, you can only add new ones. (TODO: allow select remove and update)
// All selects should be unique inside a single state. You can have same select names in different states.
// When updating state you should not change state Version - it's managed internally.
func (r *SelectRuntime) NewState(s *slct.State) error {
	// TODO: validate state here
	r.lockState(s.Id)
	defer r.unlockState(s.Id)

	old, err := r.getState(s.Id)
	if err != nil {
		return err
	}
	if old != nil && old.Version != s.Version {
		return fmt.Errorf("version mismatch")
	}
	s.Version++

	toCreate := s.NewSelects(old, nil)

	r.batchMu.Lock()
	r.ss = append(r.ss, s)
	for _, sel := range toCreate {
		r.cc = append(r.cc, sel)
	}
	cb := r.curChan
	r.batchMu.Unlock()
	<-cb

	return nil
}

type batch struct {
	wb *gorocksdb.WriteBatch
	cc chan struct{}
}

type channel struct {
	slct.Channel
	Recv *btree.BTree
	Send *btree.BTree
}

type chanSelect struct {
	State    string
	Select   string
	Group    string
	ToStatus string
	Data     []byte
	Seq      uint64
}

func (c chanSelect) Less(than btree.Item) bool {
	return c.Seq < than.(chanSelect).Seq
}

type timeSelect struct {
	State    string
	Select   string
	Group    string
	ToStatus string
	Seq      uint64
	Time     uint64
}

func (c timeSelect) Less(than btree.Item) bool {
	c2 := than.(timeSelect)
	if c.Time != c2.Time {
		return c.Time < c2.Time
	}
	return c.Seq < c2.Seq
}

func (r *SelectRuntime) lockState(id string) {
	for i := 0; ; i++ { // TODO: limit lock wait!?
		r.stateMu.Lock()
		_, locked := r.states[id]
		if !locked {
			r.states[id] = struct{}{}
			r.stateMu.Unlock()
			return
		}
		time.Sleep(time.Millisecond * 100)
	}
}

func (r *SelectRuntime) unlockState(id string) {
	r.stateMu.Lock()
	delete(r.states, id)
	r.stateMu.Unlock()
}

func (r *SelectRuntime) getState(id string) (*slct.State, error) {
	item, err := r.db.GetCF(r.ro, r.cfhStates, []byte(id)) // idempotency if request was already processed
	if err != nil {
		return nil, fmt.Errorf("get state: %v", err)
	}
	if !item.Exists() {
		return nil, nil
	}
	var s slct.State
	err = proto.Unmarshal(item.Data(), &s)
	if err != nil {
		return nil, err
	}
	item.Free()
	return &s, nil
}

func (r *SelectRuntime) removeSelect(s *slct.Select) {
	delete(r.Selects, sid{State: s.State, Select: s.Id})
	for _, c := range s.Cases {
		if c.Op == slct.Case_Send {
			r.Chans[c.Chan].Send.Delete(chanSelect{
				Seq: s.Seq,
			})
			if r.Chans[c.Chan].Send.Len() == 0 &&
				r.Chans[c.Chan].Recv.Len() == 0 {
				delete(r.Chans, c.Chan) // free up memory
			}
		}
		if c.Op == slct.Case_Recv {
			r.Chans[c.Chan].Recv.Delete(chanSelect{
				Seq: s.Seq,
			})
			if r.Chans[c.Chan].Send.Len() == 0 &&
				r.Chans[c.Chan].Recv.Len() == 0 {
				delete(r.Chans, c.Chan) // free up memory
			}
		}
		if c.Op == slct.Case_Time {
			r.TimeChans.Delete(timeSelect{
				Seq:  s.Seq,
				Time: c.Time,
			})
		}
	}
}

func (r *SelectRuntime) addSelect(s *slct.Select) {
	r.Selects[sid{State: s.State, Select: s.Id}] = s
	for _, c := range s.Cases {
		if c.Op == slct.Case_Send {
			ch, ok := r.Chans[c.Chan]
			if !ok {
				ch = channel{
					Channel: slct.Channel{
						Id: c.Chan,
					},
					Send: btree.New(32),
					Recv: btree.New(32),
				}
				r.Chans[c.Chan] = ch
			}
			ch.Send.ReplaceOrInsert(chanSelect{
				Group:    s.Group,
				State:    s.State,
				Select:   s.Id,
				ToStatus: c.ToStatus,
				Data:     c.Data,
				Seq:      s.Seq,
			})
		}
		if c.Op == slct.Case_Recv {
			ch, ok := r.Chans[c.Chan]
			if !ok {
				ch = channel{
					Channel: slct.Channel{
						Id: c.Chan,
					},
					Send: btree.New(32),
					Recv: btree.New(32),
				}
				r.Chans[c.Chan] = ch
			}
			ch.Recv.ReplaceOrInsert(chanSelect{
				Group:    s.Group,
				State:    s.State,
				Select:   s.Id,
				ToStatus: c.ToStatus,
				Seq:      s.Seq,
			})
		}
		if c.Op == slct.Case_Time {
			r.TimeChans.ReplaceOrInsert(timeSelect{
				Group:    s.Group,
				State:    s.State,
				Select:   s.Id,
				ToStatus: c.ToStatus,
				Seq:      s.Seq,
				Time:     c.Time,
			})
		}
	}
}

func (r *SelectRuntime) handleTick(wb *gorocksdb.WriteBatch) {
	now := uint64(time.Now().Unix())
	for {
		min := r.TimeChans.Min()
		if min == nil {
			return
		}
		p := min.(timeSelect)
		if p.Time > now {
			return
		}
		s := r.Selects[sid{State: p.State, Select: p.Select}]
		s.Status = slct.Select_OK
		s.ToStatus = p.ToStatus
		s.Seq = r.seq
		wb.DeleteCF(r.cfhBlockedSelects, s.BKey())
		wb.PutCF(r.cfhUnblockedSelects, s.UKey(), s.Data())
		r.removeSelect(s)
	}
}

func (r *SelectRuntime) handleSelect(wb *gorocksdb.WriteBatch, s *slct.Select) {
	if s.Status == slct.Select_OK { // client added already resolved select
		r.seq++
		s.Seq = r.seq
		wb.PutCF(r.cfhUnblockedSelects, s.UKey(), s.Data())
		return
	}

	for _, c := range s.Cases {
		if c.Op == slct.Case_Default {
			s.Status = slct.Select_OK
			s.ToStatus = c.ToStatus
			r.seq++
			s.Seq = r.seq
			wb.PutCF(r.cfhUnblockedSelects, s.UKey(), s.Data())
			return
		}
		if c.Op == slct.Case_Time {
			if c.Time > uint64(time.Now().Unix()) {
				continue
			}
			s.Status = slct.Select_OK
			s.ToStatus = c.ToStatus
			r.seq++
			s.Seq = r.seq
			wb.PutCF(r.cfhUnblockedSelects, s.UKey(), s.Data())
			return
		}

		ch, ok := r.Chans[c.Chan]
		if ch.Closed {
			s.Status = slct.Select_Closed
			s.ToStatus = c.ToStatus
			r.seq++
			s.Seq = r.seq
			wb.PutCF(r.cfhUnblockedSelects, s.UKey(), s.Data())
			return
		}
		if !ok && c.Op != slct.Case_Close {
			continue
		}
		if !ok && c.Op == slct.Case_Close {
			var ss []chanSelect
			ch.Recv.Ascend(func(item btree.Item) bool {
				p := item.(chanSelect)
				ss = append(ss, p)
				return true
			})
			ch.Send.Ascend(func(item btree.Item) bool {
				p := item.(chanSelect)
				ss = append(ss, p)
				return true
			})
			for _, p := range ss {
				s := r.Selects[sid{State: p.State, Select: p.Select}]
				s.Status = slct.Select_Closed
				s.ToStatus = p.ToStatus
				r.removeSelect(s)
				wb.DeleteCF(r.cfhBlockedSelects, s.BKey())
				r.seq++
				s.Seq = r.seq
				wb.PutCF(r.cfhUnblockedSelects, s.UKey(), s.Data())
			}
			ch.Recv = nil
			ch.Send = nil
			ch.Closed = true
			wb.PutCF(r.cfhChannels, []byte(c.Chan), ch.Channel.Data())
		}

		if c.Op == slct.Case_Recv {
			if ch.Send.Len() == 0 {
				continue
			}
			p := ch.Send.DeleteMin().(chanSelect)
			s.Status = slct.Select_OK
			s.ToStatus = c.ToStatus
			s.Peer = p.Select
			s.RecvData = p.Data

			s2 := r.Selects[sid{State: p.State, Select: p.Select}]
			s2.Status = slct.Select_OK
			s.ToStatus = p.ToStatus
			s2.Peer = s.Id
			wb.DeleteCF(r.cfhBlockedSelects, s2.BKey())
			r.removeSelect(s2)
			r.seq++
			s.Seq = r.seq
			r.seq++
			s2.Seq = r.seq
			wb.PutCF(r.cfhUnblockedSelects, s.UKey(), s.Data())
			wb.PutCF(r.cfhUnblockedSelects, s2.UKey(), s2.Data())
			return
		}

		if c.Op == slct.Case_Send {
			if ch.Recv.Len() == 0 {
				continue
			}
			p := ch.Recv.DeleteMin().(chanSelect)
			s.Status = slct.Select_OK
			s.ToStatus = c.ToStatus
			s.Peer = p.Select

			s2 := r.Selects[sid{State: p.State, Select: p.Select}]
			s2.Status = slct.Select_OK
			s.ToStatus = p.ToStatus
			s2.Peer = s.Id
			s2.RecvData = c.Data
			wb.DeleteCF(r.cfhBlockedSelects, s2.BKey())
			r.removeSelect(s2)
			r.seq++
			s.Seq = r.seq
			r.seq++
			s2.Seq = r.seq
			wb.PutCF(r.cfhUnblockedSelects, s.UKey(), s.Data())
			wb.PutCF(r.cfhUnblockedSelects, s2.UKey(), s2.Data())
			return
		}
	}

	r.seq++
	s.Seq = r.seq
	r.addSelect(s)
	wb.PutCF(r.cfhBlockedSelects, s.BKey(), s.Data())
}

func buint64(v uint64) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, v)
	return b
}

func (r *SelectRuntime) mustGetState(id string) slct.State {
	var s slct.State
	item, err := r.db.GetCF(r.ro, r.cfhStates, []byte(id))
	if err != nil {
		panic(err)
	}
	defer item.Free()
	err = proto.Unmarshal(item.Data(), &s)
	if err != nil {
		panic(err)
	}
	return s
}
